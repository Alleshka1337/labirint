﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labirint
{
    public partial class Adventure : Form
    {
        private int N = 10;

        // Лабиринт
        private StructLab Lab = null;
        private GenLab GenAl;

        // Человек
        private human human = null;


        // Ключи
        private Key key;
        private FalseKey failKey1, failKey2, failKey3;
        private Amnesia amn;

        private Random T = new Random();
        
        // Инвентарь
        private Inventory Inv=null;

        public Adventure()
        {
            InitializeComponent();
            textBox1.Text = "";
            textBox2.Enabled = false;

            textBox2.Text = "Передвижение: Стрелки клавиатуры или WASD" + Environment.NewLine;
            textBox2.Text += "Выбрать предмет из инвентаря: Двойной клик" + Environment.NewLine;
            textBox2.Text += "Убрать предмет из рук в инвентарь: Пробел" + Environment.NewLine;
            textBox2.Text += "Shift + направление: Взорвать стену" + Environment.NewLine;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Inv = new Inventory();
            Lab = new StructLab(Convert.ToInt32(N));

            GenAl = new GenLab(Lab, checkBox1.Checked);
            GenAl.GenAlg();

            textBox1.Text = "";

            int x, y;

            Lab.CellNull(); // Нигде ничего нет

            // Ставим ложный ключ Id = -2;
            NotVisit(out x, out y);
            failKey1 = new FalseKey(T.Next(0, Lab.GetSize() - 1), T.Next(0, Lab.GetSize() - 1));

            NotVisit(out x, out y);
            failKey2 = new FalseKey(T.Next(0, Lab.GetSize() - 1), T.Next(0, Lab.GetSize() - 1));

            NotVisit(out x, out y);
            failKey3 = new FalseKey(T.Next(0, Lab.GetSize() - 1), T.Next(0, Lab.GetSize() - 1));

            Lab.GetCell(failKey1.GetPosition().X, failKey1.GetPosition().Y).SetVisit(true, failKey1.GetID());
            Lab.GetCell(failKey2.GetPosition().X, failKey2.GetPosition().Y).SetVisit(true, failKey2.GetID());
            Lab.GetCell(failKey3.GetPosition().X, failKey3.GetPosition().Y).SetVisit(true, failKey3.GetID());


            // Ставим настоящий ключ Id = -1;
            NotVisit(out x, out y);
            key = new Key(T.Next(0, Lab.GetSize() - 1), T.Next(0, Lab.GetSize() - 1));
            Lab.GetCell(key.GetPosition().X, key.GetPosition().Y).SetVisit(true, key.GetID());

            // Ставим ловушку
            NotVisit(out x, out y);
            amn = new Amnesia(x, y);
            Lab.GetCell(amn.GetPosition().X, amn.GetPosition().Y).SetVisit(true, amn.GetID());


            // Создаём человека Id = 1;
            NotVisit(out x, out y);
            human = new human(x, y);
            human.Circle(x, y, Lab);            

            pictureBox1.Refresh();
            pictureBox2.Refresh();
            textBox1.Focus();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (Lab != null)
            {
               // int x, y;

                Font font = new Font(this.Font, FontStyle.Bold);

                Lab.paintLab(N, e, key, font, checkBox2.Checked); // Рисуем лабиринт с расставленными ключами

                // Рисуем человека
                if (human != null)
                {
                    human.print(human, e);
                }

                textBox1.Text = "Эффекты: " + Environment.NewLine;
                if (human != null)
                {
                    if ((human.GetKey()) || (human.GetFalseKey())) textBox1.Text += "У вас в руках ключ" + Environment.NewLine;
                }
                textBox1.Text += "Количество гранат: " + human.GetBreak() + Environment.NewLine;
            }

            textBox1.Focus();
        }


        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            EventArgs k = new EventArgs();

            if (e.Shift == false)
            {
                if ((e.KeyValue == 37) || (e.KeyValue == 65)) GoLeft();
                if ((e.KeyValue == 38) || (e.KeyValue == 87)) GoUp();
                if ((e.KeyValue == 39) || (e.KeyValue == 68)) GoRight();
                if ((e.KeyValue == 40) || (e.KeyValue == 83)) GoDown();
            }
            else
            {
                if ((e.KeyValue == 37) || (e.KeyValue == 65)) human.BreakWall(4, Lab);
                if ((e.KeyValue == 38) || (e.KeyValue == 87)) human.BreakWall(1, Lab);
                if ((e.KeyValue == 39) || (e.KeyValue == 68)) human.BreakWall(2, Lab);
                if ((e.KeyValue == 40) || (e.KeyValue == 83)) human.BreakWall(3, Lab);
            }

            if (e.KeyValue == 32) hide();
            //  textBox1.Text = Convert.ToString(e.KeyValue) + Environment.NewLine;
            pictureBox1.Refresh();
            textBox1.Focus();
        }

        private void pictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Lab.AllVision(true);
            pictureBox1.Refresh();
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black);

            if (this.Inv != null)
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        e.Graphics.DrawImage(Inv.SearchIvent(i, j), (i*50), (j*50));
                    }
                }
            }
        }

        private void pictureBox2_MouseClick(object sender, MouseEventArgs e)
        {

            if (Inv != null)
            {
                int x, y;

                x = (int)(e.X / 50); if (x > N) x = N;
                y = (int)(e.Y / 50); if (y > N) y = N;

                // Убираем то, что сейчас
                hide();

                // Достаём из рюкзака
                Inv.PickIvent(x, y, human);
            }

            pictureBox2.Refresh();
            pictureBox1.Refresh();
        }

        // Свои методы  


        // Проверка ячейки на пустоту
        private void NotVisit(out int x, out int y)
        {
            while (true)
            {
                x = T.Next(0, Convert.ToInt32(N));
                y = T.Next(0, Convert.ToInt32(N));
                if (Lab.GetCell(x, y).Get_Info_Visit() == false) break; // Если на ячейке ничего нет - норм
            }
        }

        /// <summary>
        /// Прячет содержимое рук в рюкзак
        /// </summary>
        private void hide()
        {
            // Если в руках что-то есть
            if (human.GetFalseKey() == true)
            {
                human.SetFalseKey(false);
                Inv.AddIvent(failKey1.GetID());
            }

            if (human.GetKey() == true)
            {
                human.SetKey(false);
                Inv.AddIvent(key.GetID());
            }

            // MessageBox.Show("Вы освободили руку");
            pictureBox2.Refresh();

            textBox1.Focus();
        }

        /// <summary>
        /// Производит следующий ход
        /// </summary>
        /// <param name="route"></param>
        private void Step(int route)
        {
            object sender = new object();
            EventArgs e = new EventArgs();

            switch (human.IsExit(route, Lab)) // Проверка на выход
            {
                case 0: // Если идём не в сторону выхода
                    {
                        if (human.NextStep(route, Lab) == true) // Если переход возможен
                        {
                            human.GoNext(route, Lab);
                            human.Circle(human.GetPosition().X, human.GetPosition().Y, Lab);
                            human.Activate(Lab, Inv);
                        }
                        pictureBox1.Refresh();
                        pictureBox2.Refresh();
                        break;
                    }

                case -1: // Если выход заблокировали
                    {
                        pictureBox1.Refresh();
                        break;
                    }
                case -2: // Если нет ключа
                    {
                        pictureBox1.Refresh();
                        break;
                    }
                case 1: // Если ключ был и вышли
                    {
                        Lab.AllVision(true);
                        pictureBox1.Refresh();
                        MessageBox.Show("Обновление карты");
                        button1_Click(sender, e);
                        break;
                    }
            }
        }

        // Указывают направление перехода
        private void GoUp()
        {
            Step(1);
        }

        private void GoRight()
        {
            Step(2);
        }

        private void GoDown()
        {
            Step(3);
        }

        private void GoLeft()
        {
            Step(4);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
            textBox1.Focus();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
