﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

using System.ComponentModel;
using System.Data;




namespace Labirint
{
    class StructLab
    {

        private Cell[,] Cells; // Ячейки
        private int _size; // Размер лабиринта


        public StructLab(int size)
        {
            Init(size);
        }
        public void Init(int size)
        {
            this._size = size; // Запоминаем размер
            Cells = new Cell[_size, _size]; // Выделяем память под ячейки

            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    Cells[i, j] = new Cell(i, j); // Создаём ячейку
                }
            }
        }


        /// <summary>
        /// Получает ячейку
        /// </summary>
        /// <param name="i">Номер строки</param>
        /// <param name="j">Номер столбца</param>
        /// <returns>Ячейка</returns>
        public Cell GetCell(int i, int j)
        {
            return Cells[i, j];
        }

        /// <summary>
        /// Возвращает размер лабиринта
        /// </summary>
        /// <returns>Размер</returns>
        public int GetSize()
        {
            return this._size;
        }


        /// <summary>
        /// Обнуляет "визит" всех ячеек
        /// </summary>
        public void CellNull()
        {
            for (int i = 0; i < _size; i++)
                for (int j = 0; j < _size; j++)
                    Cells[i, j].SetVisit(false, 0); // Нигде не были
        }

        public void AllVision(bool v)
        {
            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    Cells[i, j].Set_VisionDown(v);
                    Cells[i, j].Set_VisionLeft(v);
                    Cells[i, j].Set_VisionRight(v);
                    Cells[i, j].Set_VisionUp(v);
                    Cells[i, j].Set_Vision_Exit(v);
                    Cells[i, j].Set_Vision_Visit(v);
                    Cells[i, j].SetTravel(v);
               }
            }
        }

        public void paintLab(int N, PaintEventArgs e, Key key, Font font, bool way)
        {
            int x = 10;
            int y = 10;

            Cell temp;


            Brush brush = new SolidBrush(Color.Black);
            Brush blue = new SolidBrush(Color.Blue);

            Pen pen = new Pen(Color.Black);
            Pen redpen = new Pen(Color.Red);

            for (int i = 0; i < N; i++)
            {
                x = 10;
                for (int j = 0; j < N; j++)
                {
                   // temp = Lab.GetCell(i, j);
                    temp = Cells[i, j];

                    if ((temp.GetTravel() == true)&&(way==true)) e.Graphics.FillEllipse(blue, x+20, y+20, 10, 10);

                    if ((temp.Get_Info_Left_Wall() == true) && (temp.Get_VisionLeft() == true)) e.Graphics.DrawLine(pen, x, y, x, y + 50);
                    if ((temp.Get_Info_Right_Wall() == true) && (temp.Get_VisionRight() == true)) e.Graphics.DrawLine(pen, x + 50, y, x + 50, y + 50);

                    if ((temp.Get_Info_Up_Wall() == true) && (temp.Get_VisionUp() == true)) e.Graphics.DrawLine(pen, x, y, x + 50, y);
                    if ((temp.Get_Info_Down_Wall() == true) && (temp.Get_VisionDown() == true)) e.Graphics.DrawLine(pen, x, y + 50, x + 50, y + 50);

                    // Если тут что-то есть
                    if (temp.Get_Info_Visit() == true)
                    {
                        // Если ключ или ложный
                        if (((temp.Get_Id_Visit() == -1) || (temp.Get_Id_Visit() == -2)) && (temp.Get_Vision_Visit() == true))
                        {
                            e.Graphics.DrawImage(key.GetIcon(), x + 10, y + 10);
                        }

                   /*   //Ловушка  
                        if (temp.Get_Id_Visit() == -4)
                        {
                            e.Graphics.DrawLine(pen, x + 10, y + 10, x + 15, y + 15);
                        }*/
                    }

                    // Проверяем на наличие выхода
                    if (temp.Get_Exit() != 0)
                    {
                        // Направление выхода вверх
                        if ((temp.Get_Exit() == 1) && (temp.Get_Vision_Exit() == true))
                        {
                            e.Graphics.DrawString("ex", font, brush, x + 15, y - 13);
                            e.Graphics.DrawLine(redpen, x, y, x + 50, y);
                        }
                        if ((temp.Get_Exit() == 2) && (temp.Get_Vision_Exit() == true)) // Направление выхода вправо
                        {
                            e.Graphics.DrawString("e", font, brush, x + 53, y + 10);
                            e.Graphics.DrawString("x", font, brush, x + 53, y + 20);
                            e.Graphics.DrawLine(redpen, x + 50, y, x + 50, y + 50);
                        }
                        if ((temp.Get_Exit() == 3) && (temp.Get_Vision_Exit() == true))  // Направление выхода вниз
                        {
                            e.Graphics.DrawLine(redpen, x, y + 50, x + 50, y + 50);
                            e.Graphics.DrawString("ex", font, brush, x + 15, y + 53);
                        }
                        if ((temp.Get_Exit() == 4) && (temp.Get_Vision_Exit() == true)) // Направление влево
                        {
                            e.Graphics.DrawLine(redpen, x, y, x, y + 50);
                            e.Graphics.DrawString("e", font, brush, x - 11, y + 10);
                            e.Graphics.DrawString("x", font, brush, x - 11, y + 20);
                        }
                    }

                    x += 50;
                }
                y += 50;
            }
        }

    }
}
