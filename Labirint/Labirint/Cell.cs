﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labirint
{
    class Cell
    {
        private bool travel; // Проверяет, проходил ли по этой точке человек

        private int exit; // Является ли клетка выходом
        private bool _visionExit; // Виден ли выход

        private bool _leftWall; // Левая стена
        private bool _rightWall; // Правая стена
        private bool _upWall; // Верхняя стена
        private bool _downWall; // Нижняя стена
         
        private bool _visionLeft; // Видимость левой стены
        private bool _visionRight; // Видимость правой стены
        private bool _visionUp; // Видимость Верхней стены
        private bool _visionDown; // Видимость нижней стены

        // Позиция (нужна для генерации)
        private int _posX;
        private int _posY;

        // Используем для построения и для проверки, что там стоит
        private bool _visit; // Посещена ли ячейка
        private bool _visionVisit; // Видим, ли что стоит там
        private int _idVisitd; // Id элемента, стоячегона ячейке


        public Cell(int i, int j)
        {
            Init(i, j);
        }
        public void Init(int i, int j)
        {
            // Нас тут не было
            _visit = false;
            _visionVisit = false;

            // Стенки
            _leftWall = true;
            _rightWall = true;
            _upWall = true;
            _downWall = true;

            // Позиция
            _posX = i;
            _posY = j;

            // Ничего нет
            _idVisitd = 0;

            // Ничего не видим
            _visionDown = false;
            _visionLeft = false;
            _visionRight = false;
            _visionUp = false;

            // Выхода нет
            exit = 0;
            _visionExit = false;
        }


        // Посещена ли ячейка, когда ищем путь (пусть будет паблик)
        public bool wayVisit;



        //-------Посечение ячейки
        /// <summary>
        /// Проверка на посещение ячейки
        /// </summary>
        /// <returns></returns>
        public bool Get_Info_Visit()
        {
            return _visit;
        }

        // Что там стоит
        public int Get_Id_Visit()
        {
            return _idVisitd;
        }


        /// <summary>
        /// Делает ячейку посещённой
        /// </summary>
        public void Visit()
        {
            _visit = true;
        }


        // Делаем посещённой
        public void SetVisit(bool v, int id)
        {
            _idVisitd = id;
            _visit = v;
            if (v == false) _idVisitd = 0;
        }

        public bool Get_Vision_Visit()
        {
            return _visionVisit;
        }

        public void Set_Vision_Visit(bool v)
        {
            _visionVisit = v;
        }


        public bool GetTravel()
        {
            return this.travel;
        }
        public void SetTravel(bool v)
        {
            this.travel = v;
        }

        //------- Работа со стенками

        // Проврека на наличие стен
        /// <summary>
        /// Смотрит, есть ли стена слева
        /// </summary>
        /// <returns></returns>
        public bool Get_Info_Left_Wall()
        {
            return _leftWall;
        }
        /// <summary>
        /// Смотрит, есть ли стена справа
        /// </summary>
        /// <returns></returns>
        public bool Get_Info_Right_Wall()
        {
            return this._rightWall;
        }
        /// <summary>
        /// Смотрит, есть ли стена сверху
        /// </summary>
        /// <returns></returns>
        public bool Get_Info_Up_Wall()
        {
            return this._upWall;
        }
        /// <summary>
        /// Смотрит, есть ли стена снизу
        /// </summary>
        /// <returns></returns>
        public bool Get_Info_Down_Wall()
        {
            return this._downWall;
        }

        // Снос стен
        /// <summary>
        /// Сносит стену слева
        /// </summary>
        public void Break_Left()
        {
            this._leftWall = false;
        }
        /// <summary>
        /// Стосит стену справа
        /// </summary>
        public void Break_Right()
        {
            this._rightWall = false;
        }
        /// <summary>
        /// Сносит стену сверху
        /// </summary>
        public void Break_Up()
        {
            this._upWall = false;
        }
        /// <summary>
        /// Сносит стену снизу
        /// </summary>
        public void Break_Down()
        {
            this._downWall = false;
        }

        // Ставим стену
        public void Buidl(int r)
        {
            switch (r)
            {
                case 1:
                    {
                        Build_Up();
                        break;
                    }
                case 2:
                    {
                        Build_Right();
                        break;
                    }
                case 3:
                    {
                        Build_Down();
                        break;
                    }
                case 4:
                    {
                        Build_Left();
                        break;
                    }
            }
        }
        public void Build_Left()
        {
            this._leftWall = true;
        }
        public void Build_Right()
        {
            this._rightWall = true;
        }
        public void Build_Down()
        {
            this._downWall = true;
        }
        public void Build_Up()
        {
            this._upWall = true;
        }


        // Проверяем отрисовку стен
        public bool Get_VisionLeft()
        {
            return _visionLeft;
        }
        public bool Get_VisionRight()
        {
            return _visionRight;
        }
        public bool Get_VisionUp()
        {
            return _visionUp;
        }
        public bool Get_VisionDown()
        {
            return _visionDown;
        }

        // Задаём отрисовку
        public void Set_VisionLeft(bool v)
        {
            _visionLeft = v;
        }
        public void Set_VisionRight(bool v)
        {
            _visionRight = v;
        }
        public void Set_VisionUp(bool v)
        {
            _visionUp = v;
        }
        public void Set_VisionDown(bool v)
        {
            _visionDown = v;
        }

        // Задаём направление выхода
        public void Set_Exit(int i)
        {
            exit = i;
        }

        // Смотрим направление выхода
        public int Get_Exit()
        {
            return exit;
        }
        public bool Get_Vision_Exit()
        {
            return _visionExit;
        }
        public void Set_Vision_Exit(bool v)
        {
            _visionExit = v;
        }

        // Просто так
        public string Print()
        {
            string temp="";

            temp += "[";
            if (_leftWall == true) temp += "|<- ";
            if (_upWall == true) temp += "^";
            if (_downWall == true) temp += "_";
            if (_rightWall == true) temp += "->|";
            temp += "]";
            return temp;

        }


        // Используем для генерации
        public void GetPos(out int x, out int y)
        {
            x = this._posX;
            y = this._posY;
        }
    }
}
