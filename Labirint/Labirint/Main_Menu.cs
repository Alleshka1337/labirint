﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labirint
{
    public partial class Main_Menu : Form
    {
        public Main_Menu()
        {
            InitializeComponent();

           // button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Adventure K = new Adventure();
            K.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Survival K = new Survival();
            K.ShowDialog();
        }
    }
}
