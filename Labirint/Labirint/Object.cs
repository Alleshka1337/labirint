﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Labirint
{
    // Отсюда наследуются все объекты
    class Object
    {
        protected Image Icon;

        // Позиция в лабиринте
        protected int _posX;
        protected int _posY;

        // Засвет
        protected bool _vision;

        // Идентификатор объекта
        protected int _id;

        public Object(int x, int y)
        {
            Init(x, y);
        }
        public void Init(int x, int y)
        {
            this._posX = x;
            this._posY = y;
            this._vision = false;
        }

        // Ставим объект
        public void PutObject(StructLab lab)
        {
            lab.GetCell(_posX, _posY).SetVisit(true, _id);
        }

        // Получить и вернуть позицию
        public Point GetPosition()
        {
            return new Point(_posX, _posY);
        }
        public void SetPosition(int x, int y)
        {
            _posX = x;
            _posY = y;
        }

        public int GetID()
        {
            return this._id;
        }

        public bool GetVision()
        {
            return this._vision;
        }
        public void SetVision(bool v)
        {
            this._vision = v;
        }

        public Image GetIcon()
        {
            return this.Icon;
        }
    }

    // Ключ
    class Key : Object
    {
        public Key(int x, int y)
            : base(x, y)
        {
            this._id = -1;
            this.Icon = global::Labirint.Properties.Resources.key;  
        }
    }

    // Фальшивый ключ
    class FalseKey : Object
    {
        public FalseKey(int x, int y)
            : base(x, y)
        {
            this._id = -2;
        }
    }

    class Amnesia : Object
    {
        public Amnesia(int x, int y)
            : base(x, y)
        {
            this._id = -4;
        }
    }

    //--------------------------------------

    class LiveObject : Object
    {
        protected int _overview; // Дальность видимости
        protected int _distance; // К-во ходов
        protected int _breakwall; // К-во гранат

        protected int _dmg; // Наносимый урон
        protected int _hp; // Здоровье

        // Effects
        protected bool _slov;
        protected bool _speed;
        protected bool _key;
        protected bool _falseKey;


        public LiveObject(int i, int y)
            : base(i, y)
        {
            this._vision = true;

            _slov = false;
            _speed = true;
        }


        public int GetBreak()
        {
            return _breakwall;
        }
        public void BreakWall(int route, StructLab Lab)
        {

            if ((_posX == 0) && (route == 1) || ((_posY == 0) && (route == 4)) || ((_posY == Lab.GetSize() - 1) && (route == 2)) || (_posX == Lab.GetSize() - 1) && (route == 3))
            {
                MessageBox.Show("Невозможно");
                return;
            }

            if (this._breakwall > 0)
            {
                switch (route)
                {
                    case 1:
                        {
                            Lab.GetCell(this._posX, this._posY).Break_Up();
                            if (this._posX - 1 >= 0) Lab.GetCell(this._posX - 1, this._posY).Break_Down();
                            break;
                        }
                    case 2:
                        {
                            Lab.GetCell(this._posX, this._posY).Break_Right();

                            if (this._posY + 1 <= Lab.GetSize() - 1) Lab.GetCell(this._posX, this._posY + 1).Break_Left();
                            break;
                        }
                    case 3:
                        {
                            Lab.GetCell(this._posX, this._posY).Break_Down();
                            if (this._posX + 1 <= Lab.GetSize() - 1) Lab.GetCell(this._posX + 1, this._posY).Break_Up();
                            break;
                        }
                    case 4:
                        {
                            Lab.GetCell(this._posX, this._posY).Break_Left();

                            if (this._posY - 1 >= 0) Lab.GetCell(this._posX, this._posY - 1).Break_Right();
                            break;
                        }
                }
                this._breakwall -= 1;
            }
            else MessageBox.Show("Кончились гранаты");
        }

        // Смотрим, что мы видим
        public void Circle(int x, int y, StructLab lab)
        {
            int i;

            // Смотрим вверх
            i = 0;
            while (true)
            {
                if (i >= _overview) break;

                // Если стоим недалеко от верхней стены
                if (x - i < 0)
                {
                    // Если не выход
                    if (lab.GetCell(0, y).Get_Exit() == 0) lab.GetCell(0, y).Set_VisionUp(true); // Видим стену
                    else lab.GetCell(0, y).Set_Vision_Exit(true); // Видим выход
                    break;
                }


                // Если там что-то стоит
                if (lab.GetCell(x - i, y).Get_Info_Visit() == true)
                {
                    lab.GetCell(x - i, y).Set_Vision_Visit(true);
                }

                if (lab.GetCell(x - i, y).Get_Info_Up_Wall() == true)
                {
                    // Видим стену
                    lab.GetCell(x - i, y).Set_VisionUp(true);
                    break;
                }

                i++;
            }

            // Смотрим влево
            i = 0;
            while (true)
            {
                if (i >= _overview) break;

                // Если стоим недалеко от верхней стены
                if (y - i < 0)
                {
                    // Если не выход
                    if (lab.GetCell(x, 0).Get_Exit() == 0) lab.GetCell(x, 0).Set_VisionLeft(true); // Видим стену
                    else lab.GetCell(x, 0).Set_Vision_Exit(true); // Видим выход
                    break;
                }


                // Если там что-то стоит
                if (lab.GetCell(x, y - i).Get_Info_Visit() == true)
                {
                    lab.GetCell(x, y - i).Set_Vision_Visit(true);
                }

                if (lab.GetCell(x, y - i).Get_Info_Left_Wall() == true)
                {
                    // Видим стену
                    lab.GetCell(x, y - i).Set_VisionLeft(true);
                    break;
                }
                i++;
            }

            // Смотрим вправо
            i = 0;
            while (true)
            {
                if (i >= _overview) break;

                // Если стоим недалеко от верхней стены
                if (y + i >= lab.GetSize() - 1)
                {
                    // Если не выход
                    if (lab.GetCell(x, lab.GetSize() - 1).Get_Exit() == 0) lab.GetCell(x, lab.GetSize() - 1).Set_VisionRight(true); // Видим стену
                    else lab.GetCell(x, lab.GetSize() - 1).Set_Vision_Exit(true); // Видим выход
                    break;
                }


                // Если там что-то стоит
                if (lab.GetCell(x, y + i).Get_Info_Visit() == true)
                {
                    lab.GetCell(x, y + i).Set_Vision_Visit(true);
                }

                if (lab.GetCell(x, y + i).Get_Info_Right_Wall() == true)
                {
                    // Видим стену
                    lab.GetCell(x, y + i).Set_VisionRight(true);
                    break;
                }
                i++;
            }

            // Смотрим вниз
            i = 0;
            while (true)
            {
                if (i >= _overview) break;

                // Если стоим недалеко от верхней стены
                if (x + i >= lab.GetSize() - 1)
                {
                    // Если не выход
                    if (lab.GetCell(lab.GetSize() - 1, y).Get_Exit() == 0) lab.GetCell(lab.GetSize() - 1, y).Set_VisionDown(true); // Видим стену
                    else lab.GetCell(lab.GetSize() - 1, y).Set_Vision_Exit(true); // Видим выход
                    break;
                }


                // Если там что-то стоит
                if (lab.GetCell(x + i, y).Get_Info_Visit() == true)
                {
                    lab.GetCell(x + i, y).Set_Vision_Visit(true);
                }

                if (lab.GetCell(x + i, y).Get_Info_Down_Wall() == true)
                {
                    // Видим стену
                    lab.GetCell(x + i, y).Set_VisionDown(true);
                    break;
                }
                i++;
            }

            // Везде посмотрели, вроде всё

        }

        // Совершает шаг
        public bool NextStep(int route, StructLab Lab)
        {
            switch (route)
            {
                case 1: // Если направление наверх
                    {
                        // Если сверху есть стена
                        if (Lab.GetCell(this._posX, this._posY).Get_Info_Up_Wall() == true)
                        {
                            Lab.GetCell(this._posX, this._posY).Set_VisionUp(true); // Начинаем видеть стену
                            return false; // Выходим
                        }
                        else break;
                    }
                case 2: // Если направление вправо
                    {
                        if (Lab.GetCell(this._posX, this._posY).Get_Info_Right_Wall() == true)
                        {
                            Lab.GetCell(this._posX, this._posY).Set_VisionRight(true);
                            return false;
                        }
                        else break;
                    }
                case 3:
                    {
                        if (Lab.GetCell(this._posX, this._posY).Get_Info_Down_Wall() == true)
                        {
                            Lab.GetCell(this._posX, this._posY).Set_VisionDown(true);
                            return false;
                        }
                        else break;
                    }
                case 4:
                    {
                        if (Lab.GetCell(this._posX, this._posY).Get_Info_Left_Wall() == true)
                        {
                            Lab.GetCell(this._posX, this._posY).Set_VisionLeft(true);
                            return false;
                        }
                        else break;
                    }
            }
            return true;
        }

        // Административное
        public void UpCircle()
        {
            this._overview++;
        }

        /// <summary>
        /// Установить замедление
        /// </summary>
        /// <param name="v"></param>
        public void SetSlov(bool v)
        {
            this._slov = v;
        }
        public bool GetSlov()
        {
            return this._slov;
        }

        /// <summary>
        /// Установить ускорение
        /// </summary>
        /// <param name="v"></param>
        public void SetSpeed(bool v)
        {
            this._speed = v;
        }
        public bool GetSpeed()
        {
            return this._speed;
        }


        /// <summary>
        /// Ключ
        /// </summary>
        /// <param name="v"></param>
        public void SetKey(bool v)
        {
            this._key = v;
        }
        public bool GetKey()
        {
            return this._key;
        }

        /// <summary>
        /// Ложнык люч
        /// </summary>
        /// <param name="v"></param>
        public void SetFalseKey(bool v)
        {
            this._falseKey = v;
        }
        public bool GetFalseKey()
        {
            return this._falseKey;
        }
    }

    // Человек
    class human : LiveObject
    {
        public human(int i, int y)
            : base(i, y)
        {
            this._id = 1;

            this._breakwall = 3;
            this._distance = 1;
            this._overview = 0;

            this._vision = true;

            this._dmg = 0;
            this._hp = 120;

            this.Icon = global::Labirint.Properties.Resources.Human;
            
        }

        public int IsExit(int route, StructLab Lab)
        {
            // Если мы стоим у выхода и пошли не в его сторону или стоим не у выхода
            if ((Lab.GetCell(this._posX, this._posY).Get_Exit() != route)) return 0;
            else // Если мы вошли в выход
            {
                // Если ключ настоящий
                if (this._key == true)
                {
                    MessageBox.Show("Вы вышли из лабиринта");
                    return 1;
                }

                if (this._falseKey == true)
                {
                    MessageBox.Show("Ключ был фальшивый");
                    MessageBox.Show("Данный выход заблокирован");
                    Lab.GetCell(this._posX, this._posY).Set_Exit(0); // Закрываем выход
                    Lab.GetCell(this._posX, this._posY).Set_Vision_Exit(false); // Перестаём видеть выход

                    Lab.GetCell(this._posX, this._posY).Buidl(route); // Ставим стену

                    // Видим стену
                    if (route == 1) Lab.GetCell(this._posX, this._posY).Set_VisionUp(true); 
                    if (route == 2) Lab.GetCell(this._posX, this._posY).Set_VisionRight(true); 
                    if (route == 3) Lab.GetCell(this._posX, this._posY).Set_VisionDown(true);
                    if (route == 4) Lab.GetCell(this._posX, this._posY).Set_VisionLeft(true);

                    this._falseKey = false;

                    return -1;
                }
                else
                {
                    Lab.GetCell(this._posX, this._posY).Set_Vision_Exit(true);
                    MessageBox.Show("Найдите ключ");
                    return -2;
                }
            }
        }

        public void Activate(StructLab Lab, Inventory Ivent)
        {
            // Если что-то тут есть
            if (Lab.GetCell(this._posX, this._posY).Get_Info_Visit() == true)
            {

                // Если это ключ
                if (Lab.GetCell(this._posX, this._posY).Get_Id_Visit() == -1)
                {
                    // Теперь у нас есть ключ
                    //this._key = true;
                    Ivent.AddIvent(-1);

                    Lab.GetCell(this._posX, this._posY).SetVisit(false, -1); // Убираем ключ с ячейки
                    MessageBox.Show("Вы подобрали ключ");
                }

                // Если ложный ключ
                if (Lab.GetCell(this._posX, this._posY).Get_Id_Visit() == -2)
                {
                    // Теперь у нас есть ключ
                    //this._falseKey = true;
                    Ivent.AddIvent(-2);

                    Lab.GetCell(this._posX, this._posY).SetVisit(false, -2); // Убираем ключ с ячейки
                    MessageBox.Show("Вы подобрали ключ");
                }

                if (Lab.GetCell(this._posX, this._posY).Get_Id_Visit() == -4)
                {
                    Lab.GetCell(this._posX, this._posY).SetVisit(false, -4);
                    Lab.AllVision(false); // Забываем всю карту


                    int temp;
                    temp = this._posX;
                    this._posX = this._posY;
                    this._posY = temp;

                    MessageBox.Show("Вы провалились в туннель и потерялись");

                    Activate(Lab, Ivent); // Смотрим опять
                }
            }
        }

        public void GoNext(int route, StructLab Lab)
        {
            Lab.GetCell(this._posX, this._posY).SetTravel(true);

            switch (route)
            {
                case 1: // Направление вверх
                    {
                        this._posX -= 1;
                        break;
                    }
                case 2: // Направление вправо
                    {
                        this._posY += 1;
                        break;
                    }
                case 3:  // Вних
                    {
                        this._posX += 1;
                        break;
                    }
                case 4: // Влево
                    {
                        this._posY -= 1;
                        break;
                    }
            }
        }

        public void print(human human, PaintEventArgs e)
        {
            int x, y;

            y = human.GetPosition().X * 50 + 10; x = human.GetPosition().Y * 50 + 10;
            e.Graphics.DrawImage(human.GetIcon(), x + 10, y + 10);
        }

        // Контакт с минотавром

        // Убираем здоровье
        public void minHp(int d)
        {
            this._hp -= d;
        }

        // Отлетаем
        public void fly(int r, int d, StructLab lab)
        {
            switch (r)
            {
                    // Полетел вверх
                case 1:
                    {
                        if (_posX - d >= 0) this._posX -= d;
                        else _posX = 0;
                        break;                        
                    }
                case 2:
                    {
                        if (_posY + d < lab.GetSize() - 1) this._posY += d;
                        else _posY = lab.GetSize()-1 - 1;
                        break;
                    }
                case 3:
                    {
                        if (_posX + d < lab.GetSize() - 1) this._posX += d;
                        else _posX = lab.GetSize() - 1;
                        break;
                    }
                case 4:
                    {
                        if (_posY - d >= 0) this._posY -= d;
                        else _posY = 0;
                        break;  
                    }

            }
        }
        public void Amnesia(StructLab lab)
        {
            lab.AllVision(false); // Забываем всю карту
        }

        public int GetHp()
        {
            return this._hp;
        }
    }



    // Минотавр
    class Minotaur : LiveObject
    {
        private int F; // Частота, с которой надо обновлять путь
        private int f; // Сколько ходов осталось

        private bool vi;

        private int toX;
        private int toY;

        private Random T = new Random();

        private double creet;

        // Ставим на позицию
        public Minotaur(int x, int y, int f)
            : base(x, y)
        {
            vi = false;
            this.creet = 0.8;

            this.F = f;
            this.f = 0;

            this._id = 2;

            this._breakwall = 1000000;
            this._distance = 1;
            this._overview = 0;

            this._vision = false;

            this._dmg = 30;
            this._hp = 100;

            this.Icon = global::Labirint.Properties.Resources.Minotaur;
        }

        public void SearchHuman(human h, StructLab lab)
        {
            // Если прошли ходы
            if (f == 0)
            {
                // Ищем человека
                toX = h.GetPosition().X;
                toY = h.GetPosition().Y;
                f = F;

                for (int i = 0; i < lab.GetSize(); i++)
                {
                    for (int j = 0; j < lab.GetSize(); j++)
                    {
                        lab.GetCell(i, j).wayVisit = false;
                    }
                }
            }
            else
            {
                // Делаем переход
                f--;
                SearchWay(h, lab);
            }
        }


        public void SearchWay(human h, StructLab lab)
        {
            List<int> L = new List<int>();
            int k;
            int tempX, tempY;

            // Запоминаем, в каких координатах стоим
            tempX = this._posX;
            tempY = this._posY;

            k = Search_Neighbor(tempX, tempY, lab, ref L);

            // Если непосещённых соседей 1
            if ((k) == 1)
            {
                GoNext(L[0]);
                L.Clear();
            }
            else
            {
                // Идём случайным образом
                if (k != 0) Choose_Neighbor(ref this._posX, ref this._posY, ref L);
                else
                {
                    f = 0;
                    SearchHuman(h, lab);
                }
            }
        }

        // Ищет соседей клетки
        private int Search_Neighbor(int posX, int posY, StructLab Labir, ref List<int> L)
        {
            int count = 0;

            // Cмотрим соседа вверх
            if (posX != 0)
            {
                if ((Labir.GetCell(posX - 1, posY).wayVisit == false) && (Labir.GetCell(posX, posY).Get_Info_Up_Wall() == false))
                {
                    L.Add(1);
                    count++;
                }
            }

            // Смотрим соседа вниз
            if (posX != Labir.GetSize() - 1)
            {
                if ((Labir.GetCell(posX + 1, posY).wayVisit == false)&&(Labir.GetCell(posX, posY).Get_Info_Down_Wall()==false))
                {
                    L.Add(3);
                    count++;
                }
            }

            // Смотрим влево
            if (posY != 0)
            {
                if ((Labir.GetCell(posX, posY - 1).wayVisit == false)&&(Labir.GetCell(posX, posY).Get_Info_Left_Wall()==false))
                {
                    L.Add(4);
                    count++;
                }
            }

            if ((posY != Labir.GetSize() - 1)&&(Labir.GetCell(posX, posY).Get_Info_Right_Wall()==false))
            {
                if (Labir.GetCell(posX, posY + 1).Get_Info_Visit() == false)
                {
                    L.Add(2);
                    count++;
                }
            }

            // Соседи есть
            return count;
        }
        // Выбирает соседа
        private void Choose_Neighbor(ref int posX, ref int posY, ref List<int> Lis)
        {
            int n1, n2;
            n1 = posX; n2 = posY;

            int k = T.Next(0, Lis.Count);


            switch (Lis[k])
            {
                case 1:
                    {
                        posX -= 1;
                        break;
                    }
                case 2:
                    {
                        posY += 1;
                        break;
                    }
                case 3:
                    {
                        posX += 1;
                        break;
                    }
                case 4:
                    {
                        posY -= 1;
                        break;
                    }

            }

        }

        // Изменяет позицию
        private void GoNext(int route)
        {
            switch (route)
            {
                case 1: // Направление вверх
                    {
                        this._posX -= 1;
                        break;
                    }
                case 2: // Направление вправо
                    {
                        this._posY += 1;
                        break;
                    }
                case 3:  // Вних
                    {
                        this._posX += 1;
                        break;
                    }
                case 4: // Влево
                    {
                        this._posY -= 1;
                        break;
                    }
            }
        }

        public void Next(human h, StructLab lab)
        {
            SearchHuman(h, lab);
        }

        public void print(Minotaur min, PaintEventArgs e)
        {
            int x, y;

            y = min.GetPosition().X * 50 + 10; x = min.GetPosition().Y * 50 + 10;
            e.Graphics.DrawImage(min.GetIcon(), x + 10, y + 10);
        }


        // Контакт с человком
        public void Activate(human h, StructLab l)
        {
            // Если нашли человека
            if ((this._posX == h.GetPosition().X) && (this._posY == h.GetPosition().Y))
            {
                int t = T.Next(0, 5); // Направление
                double var = T.NextDouble(); // Вероятность крита
                int dist = T.Next(1, 7); // Дальность

                MessageBox.Show("Вас откинуло");

                h.fly(t, dist, l);
                if (var >= this.creet)
                {
                    h.Amnesia(l); // Шанс крита
                    h.minHp(this._dmg + Convert.ToInt32(this._dmg*0.5)); // Наносим урон
                }
                else  h.minHp(this._dmg); // Наносим урон
                this.f = 0; // Будет сон 1 ход
            }


        }

        public void angry(StructLab Lab)
        {
            int route = T.Next(1, 4);

            if ((_posX == 0) && (route == 1) || ((_posY == 0) && (route == 4)) || ((_posY == Lab.GetSize() - 1) && (route == 2)) || (_posX == Lab.GetSize() - 1) && (route == 3)) return;
            if(T.NextDouble()>=0.7) this.BreakWall(route, Lab);
        }

        public void vision(StructLab l, human h)
        {
            if ((Math.Abs(h.GetPosition().X - _posX) <= 2) && (Math.Abs(h.GetPosition().Y - _posY) <= 2))
            {
                this.vi = true;
            }
            else this.vi = false;
        }

        public bool GetVi()
        {
            return this.vi;
        }

        public void SetVi(bool v)
        {
            this.vi = v;
        }
    }

    // Инвентарь
    class Inventory
    {
        private int[,] Ivent; // Тут наш инвентарь
        private Image[,] IconIvent;
 
        public Inventory()
        {
            Ivent = new int[5,5];
            IconIvent = new Image[5, 5];

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Ivent[i,j] = 0; // У нас пустой инвентарь
                    IconIvent[i, j] = global::Labirint.Properties.Resources.NoIcon;
                }
            }
        }

        // Добавляем в инвентарь
        public void AddIvent(int id)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (Ivent[j, i] == 0)
                    {
                        Ivent[j, i] = id;
                        if ((id == -1) || (id == -2))
                        {
                            IconIvent[j, i] = global::Labirint.Properties.Resources.keyIvent1;
                        }
                        return;
                    }
                }
            }
        }

        public bool PickIvent(int x, int y, human men)
        {

            if (Ivent[x, y] == 0)
            {
                MessageBox.Show("Ничего нет");
                return false;
            }

            // Если ключ
            if (Ivent[x, y] == -1)
            {
                Ivent[x, y] = 0;
                IconIvent[x, y] = global::Labirint.Properties.Resources.NoIcon;
                men.SetKey(true);
               // MessageBox.Show("Вы взяли ключ в руки");
                return true;
            }

            // Если ложный ключ
            if (Ivent[x, y] == -2)
            {
                Ivent[x, y] = 0;
                IconIvent[x, y] = global::Labirint.Properties.Resources.NoIcon;
                men.SetFalseKey(true);
                //MessageBox.Show("Вы взяли ключ в руки");
                return true;
            }
            return true;
        }

        public Image SearchIvent(int x, int y)
        {
            return IconIvent[x, y];
        }
    }
}


