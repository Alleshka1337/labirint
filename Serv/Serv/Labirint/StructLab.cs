﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serv
{
    class StructLab
    {
        private Cell[,] Cells; // Ячейки
        private int _size; // Размер лабиринта

        public StructLab(int size)
        {
            Init(size);
        }

        public void Init(int size)
        {
            this._size = size; // Запоминаем размер
            Cells = new Cell[_size, _size]; // Выделяем память под ячейки

            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    Cells[i, j] = new Cell(i, j); // Создаём ячейку
                }
            }
        }

        /// <summary>
        /// Получает ячейку
        /// </summary>
        /// <param name="i">Номер строки</param>
        /// <param name="j">Номер столбца</param>
        /// <returns>Ячейка</returns>
        public Cell GetCell(int i, int j)
        {
            return Cells[i, j];
        }

        /// <summary>
        /// Возвращает размер лабиринта
        /// </summary>
        /// <returns>Размер</returns>
        public int GetSize()
        {
            return this._size;
        }

        /// <summary>
        /// Убирает все объекты
        /// </summary>
        public void CellNull()
        {
            for (int i = 0; i < _size; i++)
                for (int j = 0; j < _size; j++)
                    Cells[i, j].SetObject(0);
        }

        public void AllVision(bool v)
        {
            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    Cells[i, j].Set_VisionDown(v);
                    Cells[i, j].Set_VisionLeft(v);
                    Cells[i, j].Set_VisionRight(v);
                    Cells[i, j].Set_VisionUp(v);
                    Cells[i, j].Set_Vision_Exit(v);
                    Cells[i, j].Set_Vision_Object(v);
                    Cells[i, j].SetTravel(v);
                }
            }
        }


    }
}
