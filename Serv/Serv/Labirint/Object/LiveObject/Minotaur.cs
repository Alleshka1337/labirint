﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serv
{
    class Minotaur:LiveObject
    {
        private int F; // Частота, с которой надо обновлять путь
        private int f; // Сколько ходов осталось

        private bool vi;

        private int toX;
        private int toY;

        private Random T = new Random();

        private double creet;

        // Ставим на позицию
        public Minotaur(int x, int y, int grenade, int dist, int over, int dm, int hp, int f)
            : base(x, y, grenade, dist, over, dm, hp)
        {
            vi = false;
            this.creet = 0.8;

            this.F = f;
            this.f = 0;

            this._id = 2;
        }


        public void SearchHuman(human h, StructLab lab)
        {
            // Если прошли ходы
            if (f == 0)
            {
                // Ищем человека
                toX = h.GetPosition().X;
                toY = h.GetPosition().Y;
                f = F;

            }
            else
            {
                // Делаем переход
                f--;
                SearchWay(h, lab);
            }
        }


        public void SearchWay(human h, StructLab lab)
        {
            List<int> L = new List<int>();
            int k;
            int tempX, tempY;

            // Запоминаем, в каких координатах стоим
            tempX = this._posX;
            tempY = this._posY;

            k = Search_Neighbor(tempX, tempY, lab, ref L);

            // Если непосещённых соседей 1
            if ((k) == 1)
            {
                GoNext(L[0]);
                L.Clear();
            }
            else
            {
                // Идём случайным образом
                if (k != 0) Choose_Neighbor(ref this._posX, ref this._posY, ref L);
                else
                {
                    f = 0;
                    SearchHuman(h, lab);
                }
            }
        }

        // Ищет соседей клетки
        private int Search_Neighbor(int posX, int posY, StructLab Labir, ref List<int> L)
        {
            int count = 0;

            // Cмотрим соседа вверх
            if (posX != 0)
            {
                if ((Labir.GetCell(posX - 1, posY).Get_Info_Visit() == false) && (Labir.GetCell(posX, posY).Get_Info_Up_Wall() == false))
                {
                    L.Add(1);
                    count++;
                }
            }

            // Смотрим соседа вниз
            if (posX != Labir.GetSize() - 1)
            {
                if ((Labir.GetCell(posX + 1, posY).Get_Info_Visit() == false) && (Labir.GetCell(posX, posY).Get_Info_Down_Wall() == false))
                {
                    L.Add(3);
                    count++;
                }
            }

            // Смотрим влево
            if (posY != 0)
            {
                if ((Labir.GetCell(posX, posY - 1).Get_Info_Visit() == false) && (Labir.GetCell(posX, posY).Get_Info_Left_Wall() == false))
                {
                    L.Add(4);
                    count++;
                }
            }

            if ((posY != Labir.GetSize() - 1) && (Labir.GetCell(posX, posY).Get_Info_Right_Wall() == false))
            {
                if (Labir.GetCell(posX, posY + 1).Get_Info_Visit() == false)
                {
                    L.Add(2);
                    count++;
                }
            }

            // Соседи есть
            return count;
        }
        // Выбирает соседа
        private void Choose_Neighbor(ref int posX, ref int posY, ref List<int> Lis)
        {
            int n1, n2;
            n1 = posX; n2 = posY;

            int k = T.Next(0, Lis.Count);


            switch (Lis[k])
            {
                case 1:
                    {
                        posX -= 1;
                        break;
                    }
                case 2:
                    {
                        posY += 1;
                        break;
                    }
                case 3:
                    {
                        posX += 1;
                        break;
                    }
                case 4:
                    {
                        posY -= 1;
                        break;
                    }

            }

        }

        // Изменяет позицию
        private void GoNext(int route)
        {
            switch (route)
            {
                case 1: // Направление вверх
                    {
                        this._posX -= 1;
                        break;
                    }
                case 2: // Направление вправо
                    {
                        this._posY += 1;
                        break;
                    }
                case 3:  // Вних
                    {
                        this._posX += 1;
                        break;
                    }
                case 4: // Влево
                    {
                        this._posY -= 1;
                        break;
                    }
            }
        }

        public void Next(human h, StructLab lab)
        {
            SearchHuman(h, lab);
        }



        // Контакт с человком
        public void Activate(human h, StructLab l)
        {
            // Если нашли человека
            if ((this._posX == h.GetPosition().X) && (this._posY == h.GetPosition().Y))
            {
                int t = T.Next(0, 5); // Направление
                double var = T.NextDouble(); // Вероятность крита
                int dist = T.Next(1, 7); // Дальность

                h.fly(t, dist, l);
                if (var >= this.creet)
                {
                    h.Amnesia(l); // Шанс крита
                    h.minHp(this._dmg + Convert.ToInt32(this._dmg * 0.5)); // Наносим урон
                }
                else h.minHp(this._dmg); // Наносим урон
                this.f = 0; // Будет сон 1 ход
            }


        }

        public void angry(StructLab Lab)
        {
            int route = T.Next(1, 4);

            if ((_posX == 0) && (route == 1) || ((_posY == 0) && (route == 4)) || ((_posY == Lab.GetSize() - 1) && (route == 2)) || (_posX == Lab.GetSize() - 1) && (route == 3)) return;
            if (T.NextDouble() >= 0.7) this.BreakWall(route, Lab);
        }

        public void vision(StructLab l, human h)
        {
            if ((Math.Abs(h.GetPosition().X - _posX) <= 2) && (Math.Abs(h.GetPosition().Y - _posY) <= 2))
            {
                this.vi = true;
            }
            else this.vi = false;
        }

        public bool GetVi()
        {
            return this.vi;
        }

        public void SetVi(bool v)
        {
            this.vi = v;
        }
    }
}
