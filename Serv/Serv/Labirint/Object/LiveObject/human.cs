﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serv
{
    class human:LiveObject
    {

        /// <summary>
        /// Координаты
        /// </summary>
        /// <param name="i">Номер строки</param>
        /// <param name="y">Номер столбца</param>
        /// <param name="grenade">К-во гранат</param>
        /// <param name="dist">К-во ходов</param>
        /// <param name="over">Обзор</param>
        /// <param name="dm">Урон</param>
        /// <param name="hp">Здоровье</param>
        public human(int i, int y, int grenade, int dist, int over, int dm, int hp)
            : base(i, y, grenade, dist, over, dm, hp)
        {
            this._id = 1;
        }

        public int IsExit(int route, StructLab Lab)
        {
            // Если мы стоим у выхода и пошли не в его сторону или стоим не у выхода
            if ((Lab.GetCell(this._posX, this._posY).Get_Exit() != route)) return 0;
            else // Если мы вошли в выход
            {
                // Если ключ настоящий
                if (this._key == true)
                {
                    
                    return 1;
                }

                if (this._falseKey == true)
                {
                    Lab.GetCell(this._posX, this._posY).Set_Exit(0); // Закрываем выход
                    Lab.GetCell(this._posX, this._posY).Set_Vision_Exit(false); // Перестаём видеть выход

                    Lab.GetCell(this._posX, this._posY).Buidl(route); // Ставим стену

                    // Видим стену
                    if (route == 1) Lab.GetCell(this._posX, this._posY).Set_VisionUp(true);
                    if (route == 2) Lab.GetCell(this._posX, this._posY).Set_VisionRight(true);
                    if (route == 3) Lab.GetCell(this._posX, this._posY).Set_VisionDown(true);
                    if (route == 4) Lab.GetCell(this._posX, this._posY).Set_VisionLeft(true);

                    this._falseKey = false;

                    return -1;
                }
                else
                {
                    Lab.GetCell(this._posX, this._posY).Set_Vision_Exit(true);
                    return -2;
                }
            }
        }

        public void Activate(StructLab Lab, Inventory Ivent)
        {
            // Если что-то тут есть
            if (Lab.GetCell(this._posX, this._posY).Get_Id_Object()!=0)
            {

                // Если это ключ
                if (Lab.GetCell(this._posX, this._posY).Get_Id_Object() == -1)
                {
                    // Теперь у нас есть ключ
                    //this._key = true;
                    Ivent.AddIvent(-1);

                    Lab.GetCell(this._posX, this._posY).SetObject(0); // Убираем ключ с ячейки
                }

                // Если ложный ключ
                if (Lab.GetCell(this._posX, this._posY).Get_Id_Object() == -2)
                {
                    // Теперь у нас есть ключ
                    //this._falseKey = true;
                    Ivent.AddIvent(-2);

                    Lab.GetCell(this._posX, this._posY).SetObject(0); // Убираем ключ с ячейки
                 //   MessageBox.Show("Вы подобрали ключ");
                }

                if (Lab.GetCell(this._posX, this._posY).Get_Id_Object() == -4)
                {
                    Lab.GetCell(this._posX, this._posY).SetObject(0);
                    Lab.AllVision(false); // Забываем всю карту


                    int temp;
                    temp = this._posX;
                    this._posX = this._posY;
                    this._posY = temp;

                   // MessageBox.Show("Вы провалились в туннель и потерялись");

                    Activate(Lab, Ivent); // Смотрим опять
                }
            }
        }

        /// <summary>
        /// Изменяет координату
        /// </summary>
        /// <param name="route">Направление</param>
        /// <param name="Lab">Лабиринт</param>
        public void GoNext(int route, StructLab Lab)
        {
            Lab.GetCell(this._posX, this._posY).SetTravel(true);

            switch (route)
            {
                case 1: // Направление вверх
                    {
                        this._posX -= 1;
                        break;
                    }
                case 2: // Направление вправо
                    {
                        this._posY += 1;
                        break;
                    }
                case 3:  // Вних
                    {
                        this._posX += 1;
                        break;
                    }
                case 4: // Влево
                    {
                        this._posY -= 1;
                        break;
                    }
            }
        }

        // Контакт с минотавром

        // Убираем здоровье
        public void minHp(int d)
        {
            this._hp -= d;
        }
        // Отлетаем
        public void fly(int r, int d, StructLab lab)
        {
            switch (r)
            {
                // Полетел вверх
                case 1:
                    {
                        if (_posX - d >= 0) this._posX -= d;
                        else _posX = 0;
                        break;
                    }
                case 2:
                    {
                        if (_posY + d < lab.GetSize() - 1) this._posY += d;
                        else _posY = lab.GetSize() - 1 - 1;
                        break;
                    }
                case 3:
                    {
                        if (_posX + d < lab.GetSize() - 1) this._posX += d;
                        else _posX = lab.GetSize() - 1;
                        break;
                    }
                case 4:
                    {
                        if (_posY - d >= 0) this._posY -= d;
                        else _posY = 0;
                        break;
                    }

            }
        }
        public void Amnesia(StructLab lab)
        {
            lab.AllVision(false); // Забываем всю карту
        }

        public int GetHp()
        {
            return this._hp;
        }
    }
}
