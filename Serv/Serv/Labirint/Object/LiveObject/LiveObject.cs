﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serv
{
    class LiveObject:Object
    {
        protected int _overview; // Дальность видимости
        protected int _distance; // К-во ходов
        protected int _breakwall; // К-во гранат

        protected int _dmg; // Наносимый урон
        protected int _hp; // Здоровье

        // Effects
        protected bool _slov;
        protected bool _speed;
        protected bool _key;
        protected bool _falseKey;


        public LiveObject(int i, int y, int grenade, int dist, int over, int dm, int hp)
            : base(i, y)
        {
            _slov = false;
            _speed = true;

            this._breakwall = grenade;
            this._distance = dist;
            this._overview = over;


            this._dmg = dm;
            this._hp = hp;
        }


        // К-во гранат
        public int GetBreak()
        {
            return _breakwall;
        }

        // Сносим стену
        public void BreakWall(int route, StructLab Lab)
        {

            if ((_posX == 0) && (route == 1) || ((_posY == 0) && (route == 4)) || ((_posY == Lab.GetSize() - 1) && (route == 2)) || (_posX == Lab.GetSize() - 1) && (route == 3))
            {
                return;
            }

            if (this._breakwall > 0)
            {
                switch (route)
                {
                    case 1:
                        {
                            Lab.GetCell(this._posX, this._posY).Break_Up();
                            if (this._posX - 1 >= 0) Lab.GetCell(this._posX - 1, this._posY).Break_Down();
                            break;
                        }
                    case 2:
                        {
                            Lab.GetCell(this._posX, this._posY).Break_Right();

                            if (this._posY + 1 <= Lab.GetSize() - 1) Lab.GetCell(this._posX, this._posY + 1).Break_Left();
                            break;
                        }
                    case 3:
                        {
                            Lab.GetCell(this._posX, this._posY).Break_Down();
                            if (this._posX + 1 <= Lab.GetSize() - 1) Lab.GetCell(this._posX + 1, this._posY).Break_Up();
                            break;
                        }
                    case 4:
                        {
                            Lab.GetCell(this._posX, this._posY).Break_Left();

                            if (this._posY - 1 >= 0) Lab.GetCell(this._posX, this._posY - 1).Break_Right();
                            break;
                        }
                }
                this._breakwall -= 1;
            }
            else ; // Описать для вывода гранат
        }

        // Смотрим, что мы видим
        public void Circle(int x, int y, StructLab lab)
        {
            int i;

            // Смотрим вверх
            i = 0;
            while (true)
            {
                if (i >= _overview) break;

                // Если стоим недалеко от верхней стены
                if (x - i < 0)
                {
                    // Если не выход
                    if (lab.GetCell(0, y).Get_Exit() == 0) lab.GetCell(0, y).Set_VisionUp(true); // Видим стену
                    else lab.GetCell(0, y).Set_Vision_Exit(true); // Видим выход
                    break;
                }


                // Если там что-то стоит
                if (lab.GetCell(x - i, y).Get_Id_Object()!=0)
                {
                    lab.GetCell(x - i, y).Set_Vision_Object(true);
                }

                if (lab.GetCell(x - i, y).Get_Info_Up_Wall() == true)
                {
                    // Видим стену
                    lab.GetCell(x - i, y).Set_VisionUp(true);
                    break;
                }

                i++;
            }

            // Смотрим влево
            i = 0;
            while (true)
            {
                if (i >= _overview) break;

                // Если стоим недалеко от верхней стены
                if (y - i < 0)
                {
                    // Если не выход
                    if (lab.GetCell(x, 0).Get_Exit() == 0) lab.GetCell(x, 0).Set_VisionLeft(true); // Видим стену
                    else lab.GetCell(x, 0).Set_Vision_Exit(true); // Видим выход
                    break;
                }


                // Если там что-то стоит
                if (lab.GetCell(x, y - i).Get_Id_Object() != 0)
                {
                    lab.GetCell(x, y - i).Set_Vision_Object(true);
                }

                if (lab.GetCell(x, y - i).Get_Info_Left_Wall() == true)
                {
                    // Видим стену
                    lab.GetCell(x, y - i).Set_VisionLeft(true);
                    break;
                }
                i++;
            }

            // Смотрим вправо
            i = 0;
            while (true)
            {
                if (i >= _overview) break;

                // Если стоим недалеко от верхней стены
                if (y + i >= lab.GetSize() - 1)
                {
                    // Если не выход
                    if (lab.GetCell(x, lab.GetSize() - 1).Get_Exit() == 0) lab.GetCell(x, lab.GetSize() - 1).Set_VisionRight(true); // Видим стену
                    else lab.GetCell(x, lab.GetSize() - 1).Set_Vision_Exit(true); // Видим выход
                    break;
                }


                // Если там что-то стоит
                if (lab.GetCell(x, y + i).Get_Id_Object() != 0)
                {
                    lab.GetCell(x, y + i).Set_Vision_Object(true);
                }

                if (lab.GetCell(x, y + i).Get_Info_Right_Wall() == true)
                {
                    // Видим стену
                    lab.GetCell(x, y + i).Set_VisionRight(true);
                    break;
                }
                i++;
            }

            // Смотрим вниз
            i = 0;
            while (true)
            {
                if (i >= _overview) break;

                // Если стоим недалеко от верхней стены
                if (x + i >= lab.GetSize() - 1)
                {
                    // Если не выход
                    if (lab.GetCell(lab.GetSize() - 1, y).Get_Exit() == 0) lab.GetCell(lab.GetSize() - 1, y).Set_VisionDown(true); // Видим стену
                    else lab.GetCell(lab.GetSize() - 1, y).Set_Vision_Exit(true); // Видим выход
                    break;
                }


                // Если там что-то стоит
                if (lab.GetCell(x + i, y).Get_Id_Object() != 0)
                {
                    lab.GetCell(x + i, y).Set_Vision_Object(true);
                }

                if (lab.GetCell(x + i, y).Get_Info_Down_Wall() == true)
                {
                    // Видим стену
                    lab.GetCell(x + i, y).Set_VisionDown(true);
                    break;
                }
                i++;
            }

            // Везде посмотрели, вроде всё

        }

        // Совершает шаг
        public bool NextStep(int route, StructLab Lab)
        {
            switch (route)
            {
                case 1: // Если направление наверх
                    {
                        // Если сверху есть стена
                        if (Lab.GetCell(this._posX, this._posY).Get_Info_Up_Wall() == true)
                        {
                            Lab.GetCell(this._posX, this._posY).Set_VisionUp(true); // Начинаем видеть стену
                            return false; // Выходим
                        }
                        else break;
                    }
                case 2: // Если направление вправо
                    {
                        if (Lab.GetCell(this._posX, this._posY).Get_Info_Right_Wall() == true)
                        {
                            Lab.GetCell(this._posX, this._posY).Set_VisionRight(true);
                            return false;
                        }
                        else break;
                    }
                case 3:
                    {
                        if (Lab.GetCell(this._posX, this._posY).Get_Info_Down_Wall() == true)
                        {
                            Lab.GetCell(this._posX, this._posY).Set_VisionDown(true);
                            return false;
                        }
                        else break;
                    }
                case 4:
                    {
                        if (Lab.GetCell(this._posX, this._posY).Get_Info_Left_Wall() == true)
                        {
                            Lab.GetCell(this._posX, this._posY).Set_VisionLeft(true);
                            return false;
                        }
                        else break;
                    }
            }
            return true;
        }

        // Административное
        public void UpCircle()
        {
            this._overview++;
        }

        /// <summary>
        /// Установить замедление
        /// </summary>
        /// <param name="v"></param>
        public void SetSlov(bool v)
        {
            this._slov = v;
        }
        public bool GetSlov()
        {
            return this._slov;
        }

        /// <summary>
        /// Установить ускорение
        /// </summary>
        /// <param name="v"></param>
        public void SetSpeed(bool v)
        {
            this._speed = v;
        }
        public bool GetSpeed()
        {
            return this._speed;
        }


        /// <summary>
        /// Ключ
        /// </summary>
        /// <param name="v"></param>
        public void SetKey(bool v)
        {
            this._key = v;
        }
        public bool GetKey()
        {
            return this._key;
        }

        /// <summary>
        /// Ложнык люч
        /// </summary>
        /// <param name="v"></param>
        public void SetFalseKey(bool v)
        {
            this._falseKey = v;
        }
        public bool GetFalseKey()
        {
            return this._falseKey;
        }
    }
}
