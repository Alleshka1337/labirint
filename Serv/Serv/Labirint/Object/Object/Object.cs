﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Serv
{

    class Point
    {
        public int X;
        public int Y;

        public Point(int a, int b)
        {
            this.X = a;
            this.Y = b;
        }
    }

    // Отсюда наследуются все объекты
    class Object
    {
        // Позиция в лабиринте
        protected int _posX;
        protected int _posY;

        // Идентификатор объекта
        protected int _id;

        public Object(int x, int y)
        {
            Init(x, y);
        }
        public void Init(int x, int y)
        {
            this._posX = x;
            this._posY = y;
        }

        // Ставим объект
        public void PutObject(StructLab lab)
        {
            lab.GetCell(_posX, _posY).SetObject(0);
        }

        // Получить и вернуть позицию
        public Point GetPosition()
        {
            return new Point(_posX, _posY);
        }

        public void SetPosition(int x, int y)
        {
            _posX = x;
            _posY = y;
        }

        public int GetID()
        {
            return this._id;
        }
    }
}
