﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serv
{
    class Inventory
    {
        private int[,] Ivent; // Тут наш инвентарь
      //  private Image[,] IconIvent;

        public Inventory()
        {
            Ivent = new int[5, 5];
        //    IconIvent = new Image[5, 5];

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Ivent[i, j] = 0; // У нас пустой инвентарь
         //           IconIvent[i, j] = global::Labirint.Properties.Resources.NoIcon;
                }
            }
        }

        // Добавляем в инвентарь
        public void AddIvent(int id)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (Ivent[j, i] == 0)
                    {
                        Ivent[j, i] = id;
                        if ((id == -1) || (id == -2))
                        {
              //              IconIvent[j, i] = global::Labirint.Properties.Resources.keyIvent1;
                        }
                        return;
                    }
                }
            }
        }

        public bool PickIvent(int x, int y, human men)
        {

            if (Ivent[x, y] == 0)
            {
           //     MessageBox.Show("Ничего нет");
                return false;
            }

            // Если ключ
            if (Ivent[x, y] == -1)
            {
                Ivent[x, y] = 0;
            //    IconIvent[x, y] = global::Labirint.Properties.Resources.NoIcon;
                men.SetKey(true);
                // MessageBox.Show("Вы взяли ключ в руки");
                return true;
            }

            // Если ложный ключ
            if (Ivent[x, y] == -2)
            {
                Ivent[x, y] = 0;
           //     IconIvent[x, y] = global::Labirint.Properties.Resources.NoIcon;
                men.SetFalseKey(true);
                //MessageBox.Show("Вы взяли ключ в руки");
                return true;
            }
            return true;
        }

     /*   public Image SearchIvent(int x, int y)
        {
            return IconIvent[x, y];
        }*/
    }
}
