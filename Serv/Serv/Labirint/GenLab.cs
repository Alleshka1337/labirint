﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serv
{
    class GenLab
    {
        private bool _easy; // Указывает на потребность сносить дополнительные стены

        private StructLab Labir; // Наш лабиринт

        private Random T = new Random();

        private Stack<Cell> K = new Stack<Cell>(); // Хранится ячейка разветвления
        private List<int> Lis = new List<int>(); // Соседи

        public GenLab(StructLab Lab, bool easy)
        {
            Init(Lab, easy);
        }
        public void Init(StructLab Lab, bool easy)
        {
            Labir = Lab;
            this._easy = easy;
        }

        public void GenAlg()
        {
            Cell temp; // Ячейка

            int posX, posY;

            // Выбираем случайную ячейку
            posX = T.Next(0, Labir.GetSize()); 
            posY = T.Next(0, Labir.GetSize());

            temp = Labir.GetCell(posX, posY);

            // Помечаем её пройденной
            temp.Visit(true);

            while (true)
            {
                Lis.Clear(); // Очищаем список соседей

                // Если кончились непосещённые ячейки - выход
                if (End() == false) break;
                else
                {
                    // Если соседи есть
                    if (Search_Neighbor(posX, posY) == true)
                    {
                        // Запоминаем ячейку
                        K.Push(Labir.GetCell(posX, posY));

                        // Выбираем соседа и ломаем стены
                        Choose_Neighbor(ref posX, ref posY);

                        // Делаем посещённой
                        Labir.GetCell(posX, posY).Visit(true);
                    }
                    else
                    {
                        // Если стек не пуст
                        if(K.Count!=0) K.Pop().GetPos(out posX, out posY); 
                    }
                }
            }

            // Сгенерируем выходы

            // Выход слева
            posY = T.Next(0, Labir.GetSize()); // Генерим позицию
            Labir.GetCell(posY, 0).Break_Left(); // Ломаем стену
            Labir.GetCell(posY, 0).Set_Exit(4); // Ставим выход
                
            // Справа
            posY = T.Next(0, Labir.GetSize()); // Генерим позицию
            Labir.GetCell(posY,Labir.GetSize()-1).Break_Right(); // Ломаем стену
            Labir.GetCell(posY, Labir.GetSize() - 1).Set_Exit(2); // Ставим выход

            // Сверху
            posY = T.Next(0, Labir.GetSize()); // Генерим позицию
            Labir.GetCell(0, posY).Break_Up(); // Ломаем стену
            Labir.GetCell(0, posY).Set_Exit(1); // Ставим выход

            // Снизу
            posY = T.Next(0, Labir.GetSize()); // Генерим позицию
            Labir.GetCell(Labir.GetSize() - 1, posY).Break_Down(); // Ломаем стену
            Labir.GetCell(Labir.GetSize() - 1, posY).Set_Exit(3); // Ставим выход

            Labir.CellNull(); // Обнуляем все ячейки



            // Долбим доп. стены
            if (_easy == true)
            {
                int k = T.Next(0, Convert.ToInt32(Labir.GetSize()*Labir.GetSize()/2)); 
                int route;

                for (int z = 0; z<k; z++)
                {
                    do
                    {
                        posX = T.Next(1, Labir.GetSize()-2);
                        posY = T.Next(1, Labir.GetSize()-2);
                        route = T.Next(1, 4);

                        if ((route == 1) && (Labir.GetCell(posX, posY).Get_Info_Up_Wall() == true))
                        {
                            Labir.GetCell(posX, posY).Break_Up();
                            Labir.GetCell(posX-1, posY).Break_Down();
                            break;
                        }
                        if ((route == 2) && (Labir.GetCell(posX, posY).Get_Info_Right_Wall() == true))
                        {
                            Labir.GetCell(posX, posY).Break_Right();
                            Labir.GetCell(posX, posY+1).Break_Left();
                            break;
                        }
                        if ((route == 3) && (Labir.GetCell(posX, posY).Get_Info_Down_Wall() == true))
                        {
                            Labir.GetCell(posX, posY).Break_Down();
                            Labir.GetCell(posX + 1, posY).Break_Up();
                            break;
                        }
                        if ((route == 4) && (Labir.GetCell(posX, posY).Get_Info_Left_Wall() == true))
                        {
                            Labir.GetCell(posX, posY).Break_Left();
                            Labir.GetCell(posX, posY-1).Break_Right();
                            break;
                        }

                    } while (true);

                }

            }

            for (int i = 0; i < Labir.GetSize(); i++)
                for (int j = 0; j < Labir.GetSize(); j++)
                    Labir.GetCell(i, j).Visit(false);
        }

        /// <summary>
        /// Проверяет, есть ли непосещённые ячейки
        /// </summary>
        /// <returns>true - есть, false - нет</returns>
        private bool End()
        {
            Cell temp;

            for (int i = 0; i < Labir.GetSize(); i++)
            {
                for (int j = 0; j < Labir.GetSize(); j++)
                {
                    temp = Labir.GetCell(i, j);
                    if (temp.Get_Info_Visit() == false) return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Ищет соседей
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        /// <returns></returns>
        private bool Search_Neighbor(int posX, int posY)
        {
            int count=0;

            if (posX != 0)
            {
                if (Labir.GetCell(posX - 1, posY).Get_Info_Visit() == false)
                {
                    Lis.Add(1);
                    count++;
                }
            }

            if (posX != Labir.GetSize() - 1)
            {
                if (Labir.GetCell(posX + 1, posY).Get_Info_Visit() == false)
                {
                    Lis.Add(3);
                    count++;
                }
            }

            if (posY != 0)
            {
                if (Labir.GetCell(posX, posY - 1).Get_Info_Visit() == false)
                {
                    Lis.Add(4);
                    count++;
                }
            }

            if (posY != Labir.GetSize() - 1)
            {
                if (Labir.GetCell(posX, posY + 1).Get_Info_Visit() == false)
                {
                    Lis.Add(2);
                    count++;
                }
            }

            // Соседи есть
            if (count != 0) return true;
            else return false; // Соседей нет
        }

        // Выбирает соседа
        private void Choose_Neighbor(ref int posX, ref int posY)
        {
            int n1, n2;
            n1 = posX; n2 = posY;

            int k = T.Next(0, Lis.Count);

            
            switch (Lis[k])
            {
                case 1:
                    {
                        posX -= 1;
                        Labir.GetCell(n1, n2).Break_Up();
                        Labir.GetCell(posX, posY).Break_Down();
                        break;
                    }
                case 2:
                    {
                        posY += 1;
                        Labir.GetCell(n1, n2).Break_Right();
                        Labir.GetCell(posX, posY).Break_Left();
                        break;
                    }
                case 3:
                    {
                        posX += 1;
                        Labir.GetCell(n1, n2).Break_Down();
                        Labir.GetCell(posX, posY).Break_Up();
                        break;
                    }
                case 4:
                    {
                        posY -= 1;
                        Labir.GetCell(n1, n2).Break_Left();
                        Labir.GetCell(posX, posY).Break_Right();
                        break;
                    }

            }

        }
    }
}
