﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serv
{
    class Cell // Ячейка
    {
        //<------------------- Стены - Начало ----------------->
        // Описываем стены ячейки
        private bool _leftWall; // Левая стена
        private bool _rightWall; // Правая стена
        private bool _upWall; // Верхняя стена
        private bool _downWall; // Нижняя стена

        // Проверка на наличие стен
        public bool Get_Info_Left_Wall()
        {
            return _leftWall;
        }
        public bool Get_Info_Right_Wall()
        {
            return this._rightWall;
        }
        public bool Get_Info_Up_Wall()
        {
            return this._upWall;
        }
        public bool Get_Info_Down_Wall()
        {
            return this._downWall;
        }

        // Описываем видимость стен
        private bool _visionLeft; // Видимость левой стены
        private bool _visionRight; // Видимость правой стены
        private bool _visionUp; // Видимость Верхней стены
        private bool _visionDown; // Видимость нижней стены

        // Проверка на отрисовку стен
        public bool Get_VisionLeft()
        {
            return _visionLeft;
        }
        public bool Get_VisionRight()
        {
            return _visionRight;
        }
        public bool Get_VisionUp()
        {
            return _visionUp;
        }
        public bool Get_VisionDown()
        {
            return _visionDown;
        }

        // Задаём отрисовку
        public void Set_VisionLeft(bool v)
        {
            _visionLeft = v;
        }
        public void Set_VisionRight(bool v)
        {
            _visionRight = v;
        }
        public void Set_VisionUp(bool v)
        {
            _visionUp = v;
        }
        public void Set_VisionDown(bool v)
        {
            _visionDown = v;
        }

        // Ломаем стены
        public void Break_Left()
        {
            this._leftWall = false;
        }
        public void Break_Right()
        {
            this._rightWall = false;
        }
        public void Break_Up()
        {
            this._upWall = false;
        }
        public void Break_Down()
        {
            this._downWall = false;
        }

        // Ставим стены
        public void Buidl(int r)
        {
            switch (r)
            {
                case 1:
                    {
                        Build_Up();
                        break;
                    }
                case 2:
                    {
                        Build_Right();
                        break;
                    }
                case 3:
                    {
                        Build_Down();
                        break;
                    }
                case 4:
                    {
                        Build_Left();
                        break;
                    }
            }
        }
        public void Build_Left()
        {
            this._leftWall = true;
        }
        public void Build_Right()
        {
            this._rightWall = true;
        }
        public void Build_Down()
        {
            this._downWall = true;
        }
        public void Build_Up()
        {
            this._upWall = true;
        }
        //<------------------- Стены - Конец ----------------->



        // <------------------- Используется для генерации - Начало ----------------->
        private int _posX;
        private int _posY;

        private bool _visit; // Посещена ли ячейка

        public bool Get_Info_Visit()
        {
            return _visit;
        }
        public void Visit(bool v)
        {
            _visit = v;
        }

        // Используем для генерации
        public void GetPos(out int x, out int y)
        {
            x = this._posX;
            y = this._posY;
        }
        // <------------------- Используется для генерации - Конец ----------------->



        // <------------------- Для постановки объектов - Начало ----------------->
        private int _idObject; // Id элемента, стоячегона ячейке
        private bool _visionObject; // Видим, ли что стоит там

        /// <summary>
        /// Ставит объект
        /// </summary>
        /// <param name="id">Идентификатор объекта. 0 - пусто</param>
        public void SetObject(int id)
        {
            _idObject = id;
        }

        // Что там стоит
        public int Get_Id_Object()
        {
            return _idObject;
        }

        // Статус прорисовки объекта
        public bool Get_Vision_Object()
        {
            return _visionObject;
        }

        public void Set_Vision_Object(bool v)
        {
            _visionObject = v;
        }

        // <------------------- Для постановки объектов - Конец ----------------->


        // <------------------- Выходы - Начало ----------------->
        private int exit; // Является ли клетка выходом
        private bool _visionExit; // Виден ли выход

        // Задаём направление выхода
        public void Set_Exit(int i)
        {
            exit = i;
        }

        // Смотрим направление выхода

        public int Get_Exit()
        {
            return exit;
        }
        public bool Get_Vision_Exit()
        {
            return _visionExit;
        }
        public void Set_Vision_Exit(bool v)
        {
            _visionExit = v;
        }
        // <------------------- Выходы - Конец ----------------->


        // <------------------- Показ пути - Начало ----------------->
        private bool travel; // Проверяет, проходил ли по этой точке человек
        public bool GetTravel()
        {
            return this.travel;
        }
        public void SetTravel(bool v)
        {
            this.travel = v;
        }
        // <------------------- Показ пути - Конец ----------------->


        public Cell(int i, int j)
        {
            Init(i, j);
        }
        public void Init(int i, int j)
        {
            // Генерация
            this._visit = false; // Ещё не совершали обход

            this._leftWall = true;
            this._rightWall = true;
            this._upWall = true;
            this._downWall = true;

            this._posX = i;
            this._posY = j;

            // Объекты
            this._idObject = 0;
            this._visionObject = false;

            // Прорисовка стен
            _visionDown = false;
            _visionLeft = false;
            _visionRight = false;
            _visionUp = false;

            // Выход
            exit = 0;
            _visionExit = false;
        }
    }
}
